#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <time.h>

#define STATE_THINKING	0	//blocked
#define STATE_HUNGRY	1	//ready
#define STATE_EATING	2	//running

//Μέγιστος και ελάχιστος αριθμός δευτερολέπτων στα οποία ο φιλόσοφος παραμένει σε κατάσταση THINKING.
#define RANDMIN 1
#define RANDMAX 5

//Κβάντο χρόνου
#define Q 1

//Συνολικός χρόνος (σε sec) στον οποίο ο φιλόσοφος είναι σε κατάσταση EATING.
#define TOTAL_EATING_TIME 20

#define TRUE 1
#define FALSE 0

float Philosopher(int, int);
void print_State(int, int);
int modulo(int, int);


sem_t *mutex;
int *states, *finished;

int main(void)
{
	//Πλήθος φιλοσόφων.
	int num_phs;
	printf("Give me the number of philosophers (3-10):\n");
	do {
		scanf("%i", &num_phs);
		if (num_phs < 3 || num_phs > 10)
		{
			printf("Invalid Input!\n");
		}
		
	} while (num_phs < 3 || num_phs > 10);

	float * wait_time_avg;

	//Δημιουργία κοινόχρηστης μνήμης με δικαίωμα ανάγνωσης και εγγραφής από τις διεργασίες.
	mutex = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	states = mmap(NULL, num_phs * sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	finished = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	wait_time_avg = mmap(NULL, num_phs * sizeof(float), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

	//Αρχικοποίηση του σημαφόρου με την τιμή 1 και δήλωση ότι θα είναι κοινόχρηστος μεταξύ των διεργασιών.
	sem_init(mutex, 1, 1);

	int i;
	for (i = 0; i < num_phs; i++)
		states[i] = STATE_THINKING;

	//Δημιουργία των φιλοσόφων.
	*finished = 0;
	for (i = 0; i < num_phs; i++)
	{
		if (fork() == 0)
		{
			wait_time_avg[i] = Philosopher(i, num_phs);
			(*finished)++;
			exit(0);
		}
	}
	
	
	//Η γονική διεργασία περιμένει να τελειώσουν όλοι η φιλόσοφοι.
	while (*finished < num_phs);

	printf("\n");
	float total_wait_time = 0;
	for (i = 0; i < num_phs; i++)
	{
		printf("Philosopher %d average wait time was %f seconds\n", i + 1, wait_time_avg[i]);
		total_wait_time += wait_time_avg[i];
	}
	printf("Philosophers total wait time was %f seconds\n", total_wait_time);

	//Καταστροφή σημαφόρου.
	sem_destroy(mutex);

	//Απελευθέρωση της κοινόχρηστης μνήμης.
	munmap(mutex, sizeof(sem_t));
	munmap(states, num_phs * sizeof(int));
	munmap(finished, sizeof(int));
	munmap(wait_time_avg, num_phs * sizeof(float));

	return 0;
}

float Philosopher(int i, int num_phs)
{
	//Χρήση του pid ως seed για τις random λειτουργιές (χρόνος THINKING).
	srand(getpid());
	
	int eating = 0;

	float philos_wait_time_avg = 0;
	int failed_count = 0;
	int start_hungry, end_hungry;

	int left_state, right_state, state;
	
	//Προτεραιότητα εκτέλεσης.
	int T = Q * (i + 1);
	int time_thinking;

	int fail = TRUE;
	
	while(eating < TOTAL_EATING_TIME)
	{
		//Υπολογισμός κατάστασης αριστερού, δεξιού και ενεργού φιλοσόφου.
		state = states[i];

		/*
		 * Αλλαγή σε κατάσταση THINKING και υπολογισμός του τυχαίου
		 * χρόνου όπου θα παραμείνει σε αυτήν την κατάσταση.
		 *
		 * Χρησιμοποιώ τον σημαφόρο για να πάει σε κατάσταση thinking
		 * και να τυπώσει το κατάλληλο μήνυμα χωρίς να διακοπεί από άλλη
		 * διεργασία.
		 */
		sem_wait(mutex);
		states[i] = STATE_THINKING;
		time_thinking = rand() % RANDMAX + RANDMIN;
		print_State(STATE_THINKING, i);
		sem_post(mutex);

		sleep(time_thinking);

		sem_wait(mutex);
		states[i] = STATE_HUNGRY;
		print_State(STATE_HUNGRY, i);
		start_hungry = time(NULL);
		sem_post(mutex);

		while(TRUE)
		{
			left_state = states[modulo(i-1, num_phs)];
			right_state = states[modulo(i+1, num_phs)];

			sem_wait(mutex);

			if (left_state == STATE_EATING)
			{
				/*
				 * Αναγκαίος έλεγχος για να μην εμφανίζετε πολλές
				 * φορές το μήνυμα αποτυχίας για το πιρούνι.
				 */
				if(fail)
				{
					failed_count++;
					printf("Philosopher %d failed to take left fork %d, because Philosopher %d was eating\n", i+1, (modulo(i-1,num_phs)) + 1, (modulo(i-1,num_phs)) + 1);
					fail = FALSE;
				}
				sem_post(mutex);
			}
			else if (right_state == STATE_EATING)
			{
				if(fail)
				{
					failed_count++;
					printf("Philosopher %d failed to take right fork %d, because Philosopher %d was eating\n", i+1, (i + 1) % num_phs, ((i + 1) % num_phs) + 1);
					fail = FALSE;
				}
				sem_post(mutex);
			}
			else
			{
				end_hungry = time(NULL);
				philos_wait_time_avg += end_hungry - start_hungry;
				fail = TRUE;
				states[i] = STATE_EATING;
				print_State(STATE_EATING, i);
				sem_post(mutex);

				sleep(T);
				eating += T;
				break;
			}
		}
	}

	sem_wait(mutex);
	states[i] = STATE_THINKING;
	print_State(STATE_THINKING, i);
	sem_post(mutex);

	return (failed_count == 0)? 0 : philos_wait_time_avg / failed_count;
}

void print_State(int state, int i)
{
	struct tm *tm;
	time_t t;
	int hour, min, sec;

	time(&t);
	tm = localtime(&t);
	hour = tm->tm_hour, min = tm->tm_min, sec = tm->tm_sec;

	switch(state)
	{
		case STATE_EATING:
			printf("Philosopher %d is EATING at time %02d:%02d:%02d\n", i + 1, hour, min, sec);
			break;
		case STATE_HUNGRY:
			printf("Philosopher %d is HUNGRY at time %02d:%02d:%02d\n", i + 1, hour, min, sec);
			break;
		case STATE_THINKING:
			printf("Philosopher %d is THINKING at time %02d:%02d:%02d\n", i + 1, hour, min, sec);
			break;
	}
}

int modulo(int x, int N)
{
	return (x % N + N) % N;
}
